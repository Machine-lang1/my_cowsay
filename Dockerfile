FROM ubuntu

RUN apt update && apt install -y cowsay && /usr/games/cowsay /usr/bin/cowsay

ENTRYPOINT ["cowsay"]
